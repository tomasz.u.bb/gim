﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour {
	public CharacterMotor Motor;

	
	// Update is called once per frame
	void Update () {
		if (Motor != null) {
			Vector2 moveDirection = Vector2.zero;
			if (Input.GetKey (KeyCode.LeftArrow)) {
				moveDirection.x = -1.0f;
			} else if (Input.GetKey (KeyCode.RightArrow)) {
				moveDirection.x = 1.0f;
			}

			bool jump = false;
			if (Input.GetKey (KeyCode.Space)) {
				jump = true;
			}

			Motor.Move(moveDirection, jump);
		}
	}
}
