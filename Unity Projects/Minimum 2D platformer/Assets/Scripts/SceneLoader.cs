﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {
	public string SceneName = "Main Menu";

	/// <summary>
	/// Ta metoda będzie głównie wywoływana z UnityEvent z innych komponentów.
	/// Najczęściej z triggerów bądź z UI.
	/// </summary>
	public void Load(){
		SceneManager.LoadScene (SceneName);
	}
}
