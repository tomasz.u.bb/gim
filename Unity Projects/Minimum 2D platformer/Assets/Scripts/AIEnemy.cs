﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AIEnemy : MonoBehaviour {
	const float DistanceToPatrolPoint = 0.25f;

	public CharacterMotor Motor;
	public float BounceForce = 1500.0f;
	public Transform[] PatrolPoints = new Transform[0];
	public UnityEvent OnDie = new UnityEvent ();

	int patrolPointIndex = 0;

	
	// Update is called once per frame
	void Update () {
		if (Motor == null) {
			return;
		}

		if (PatrolPoints.Length < 1) {
			return;
		}

		Vector3 targetPosition = PatrolPoints [patrolPointIndex].position;
		//aby nie musieć pilnować dokładnej wysokości, przy rozstawianiu punktów patrolowych
		targetPosition.y = transform.position.y;

		if ((targetPosition - transform.position).magnitude <= DistanceToPatrolPoint) {
			patrolPointIndex = (patrolPointIndex + 1) % PatrolPoints.Length;
			targetPosition.x = PatrolPoints [patrolPointIndex].position.x;
		}

		Vector2 moveDirection = new Vector2 (
			                        (targetPosition.x - transform.position.x),
			                        0.0f
		                        );

		//mechanika ruchu zakłada, że na wejściu otrzyma zakres <-1;1>
		//większe wartości są zarezerwowane dla sprintu
		moveDirection.x = Mathf.Sign(moveDirection.x) * Mathf.Clamp01 (Mathf.Abs(moveDirection.x));

		Motor.Move (moveDirection, false);
	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == Tags.Player) {
			//jeżeli gracz naskoczył z góry, to pokonujemy przeciwnika
			bool jumpedOnFromAbove = false;

			for (int i = 0; i < other.contacts.Length; i++) {
				ContactPoint2D contactPoint = other.contacts [i];
				if (contactPoint.normal.y < -0.6f) {
					jumpedOnFromAbove = true;
					break;
				}
			}

			if (jumpedOnFromAbove) {
				if (other.rigidbody != null) {
					other.rigidbody.AddForce (Vector2.up * BounceForce);
				}

                other.gameObject.GetComponent<CharacterSounds>().PlayBounce();

				Destroy (gameObject);
			}
		}
	}

	void OnDestroy(){
		OnDie.Invoke ();
	}
}
