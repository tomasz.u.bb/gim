﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicSetup : MonoBehaviour {
    public BGMSource BGMPrefab;

    [Space]
    public AudioClip Music;

    void Awake()
    {
        if(BGMSource.Instance == null)
        {
            Instantiate(BGMPrefab);
        }

        BGMSource.Play(Music);
    }
}
