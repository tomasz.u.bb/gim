﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LevelManager : MonoBehaviour {
	public static LevelManager Instance {
		get;
		private set;
	}

	[SerializeField] UnityEvent OnLevelStart = new UnityEvent ();
	[SerializeField] UnityEvent OnPlayerDied = new UnityEvent ();

	// Use this for initialization
	void Awake () {
		Instance = this;
	}

	void Start(){
		OnLevelStart.Invoke ();
	}

	public void NotifyPlayerDied(){
		OnPlayerDied.Invoke ();
	}
}
