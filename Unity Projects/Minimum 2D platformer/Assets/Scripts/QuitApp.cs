﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitApp : MonoBehaviour {
	/// <summary>
	/// Ta metoda będzie głównie wywoływana z UnityEvent z innych komponentów.
	/// Najczęściej z triggerów bądź z UI.
	/// </summary>
	public void Load(){
		Application.Quit ();
	}
}
