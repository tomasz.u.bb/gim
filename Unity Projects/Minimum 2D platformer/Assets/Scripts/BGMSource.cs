﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMSource : MonoBehaviour {
    static public BGMSource Instance
    {
        get;
        private set;
    }

    static public void Play(AudioClip music)
    {
        if(Instance == null)
        {
            return;
        }

        var audio = Instance.GetComponent<AudioSource>();
        if(audio.clip != music)
        {
            audio.clip = music;
            audio.Play();
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        Instance = this;
    }
}
