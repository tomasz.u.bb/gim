﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Character motor.
/// 
/// Formatowanie kodu domyślne dla IDE.
/// Nie mam ochoty walczyć z ustawieniami,
/// zatem przepraszam.
/// </summary>
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class CharacterMotor : MonoBehaviour {
    const string IsGoundedAnimKey = "isGrounded";
    const string VelocityAnimKey = "velocity";

	public float JumpForce = 50.0f;
	public float MoveForce = 20.0f;
	public float AirControll = 0.25f;
	[Space]
	public Vector2 GroundOffset = Vector2.down * 1.5f;
	public float GroundCheckRadius = 0.15f;
    [Space]
    public Animator Animator;
    public CharacterSounds Sounds;

    public bool IsGrounded
    {
        get;
        private set;
    }

	Rigidbody2D m_RB;
	SpriteRenderer m_Renderer;

	Vector2 m_MoveDirection;
	bool m_Jump = false;

	void Awake () {
		m_RB = GetComponent<Rigidbody2D> ();
		m_Renderer = GetComponent<SpriteRenderer> ();
        IsGrounded = true;
	}

	void FixedUpdate () {
		//możemy sterować postacią tylko, gdy jest na ziemi
		bool isGrounded = false;
		Collider2D[] possibleGrounds = Physics2D.OverlapCircleAll (m_RB.position + GroundOffset, GroundCheckRadius);
		for (int i = 0; i < possibleGrounds.Length; i++) {
			//w okolicach stóp postaci będziemy wyłapywać też samą postać - musimy to odfiltrować
			if (possibleGrounds [i].gameObject != gameObject) {
				isGrounded = true;
				break;
			}
		}

        if(isGrounded)
        {
            if(IsGrounded == false)
            {
                Sounds.PlayLand();
            }
        }
        IsGrounded = isGrounded;

		Vector2 moveForce = m_MoveDirection * MoveForce;

		if (isGrounded == false) {
			moveForce = moveForce * AirControll;
			m_Jump = false;
		}

		m_RB.AddForce (moveForce);
		if (m_Jump) {
			//fix multiplikowania się skoku, przy gęsto rozstawionych platformach
			Vector2 velocity = m_RB.velocity;
			velocity.y = 0.0f;
			m_RB.velocity = velocity;

			//właściwy skok
			m_RB.AddForce (Vector2.up * JumpForce);
            Sounds.PlayJump();

        }

        Animator.SetBool(IsGoundedAnimKey, isGrounded);
        Animator.SetFloat(VelocityAnimKey, Mathf.Abs(m_RB.velocity.x));
	}

	void OnDrawGizmos(){
		Gizmos.DrawWireSphere (transform.position + new Vector3(GroundOffset.x, GroundOffset.y), GroundCheckRadius);
	}


	public void Move(Vector2 direction, bool jump){
		m_MoveDirection = direction;
		m_Jump = jump;

		if (direction.x > 0.1f) {
			m_Renderer.flipX = false;
		} else if (direction.x < -0.1f) {
			m_Renderer.flipX = true;
		}
	}
}
