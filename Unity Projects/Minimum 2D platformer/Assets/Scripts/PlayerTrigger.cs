﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTrigger : MonoBehaviour {
	[SerializeField] UnityEvent m_OnEntered = new UnityEvent();

	void OnTriggerEnter2D(Collider2D other){
		if (other.tag == Tags.Player) {
			m_OnEntered.Invoke ();
		}
	}
}
