﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSounds : MonoBehaviour {
    public AudioSource CharacterSoundSource;

    [Space]
    public AudioClip Footstep;
    public AudioClip Jump;
    public AudioClip Land;
    public AudioClip Bounce;

    void Reset()
    {
        CharacterSoundSource = GetComponent<AudioSource>();
    }

    public void PlayFootstep()
    {
        CharacterSoundSource.PlayOneShot(Footstep);
    }

    public void PlayJump()
    {
        CharacterSoundSource.PlayOneShot(Jump);
    }

    public void PlayLand()
    {
        CharacterSoundSource.PlayOneShot(Land);
    }

    public void PlayBounce()
    {
        CharacterSoundSource.PlayOneShot(Bounce);
    }
}
