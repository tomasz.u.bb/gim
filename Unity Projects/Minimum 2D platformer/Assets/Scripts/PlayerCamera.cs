﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {
	public Vector3 offset = Vector3.back * 10.0f;
	public float Damp = 2.0f;

	Transform m_Target = null;

	Transform Target{
		get{
			if (m_Target == null) {
				GameObject go = GameObject.FindGameObjectWithTag (Tags.Player);
				if (go != null) {
					m_Target = go.transform;
				}
			}

			return m_Target;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Target == null) {
			return;
		}

		Vector3 targetPosition = Target.position + offset;
		targetPosition = Vector3.Lerp (transform.position, targetPosition, Time.fixedDeltaTime * Damp);
		transform.position = targetPosition;
	}
}
